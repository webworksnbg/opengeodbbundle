<?php

namespace OpenGeoDb;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OpenGeoDbBundle
 * @package OpenGeoDb
 */
class OpenGeoDbBundle extends Bundle
{
}
