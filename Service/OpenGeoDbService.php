<?php

namespace OpenGeoDb\Service;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use OpenGeoDb\Helper\CollectionHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenGeoDbService
 * @package OpenGeoDb\Service
 */
class OpenGeoDbService
{
    /**
     * @var EntityManager
     */
    private $em = NULL;

    /**
     * @var Connection
     */
    private $conn = NULL;

    /**
     * OpenGeoDbService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine')->getManager('opengeodb');
        $this->conn = $this->em->getConnection();
    }

    /**
     * @param $sql
     * @return mixed
     */
    private function execute($sql)
    {
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @param $locationName
     * @return mixed
     */
    public function getBestMatchingLocationId($searchTerm)
    {
        $extendedLocationList = [];

        switch ($this->getSearchTermType($searchTerm)) {
            case 'locationName':
                $locationList = $this->getCoordinatesByName($searchTerm);
                $extendedLocationList = CollectionHelper::addLevenshteinDistance($locationList, $searchTerm, 'zc_location_name');
                break;

            case 'zipCode':
                $locationList = $this->getCoordinatesByZipCode($searchTerm);
                $extendedLocationList = CollectionHelper::addLevenshteinDistance($locationList, $searchTerm, 'zc_zip');
                break;
        }

        if (!empty($extendedLocationList)) {
            $sortedLocationList = CollectionHelper::sortByLeveshteinDistance($extendedLocationList);

            if (!empty($sortedLocationList)) {
                $locationIdCandidate = $sortedLocationList[0];

                if (array_key_exists('zc_id', $locationIdCandidate)) {
                    return $locationIdCandidate['zc_id'];
                }
            }
        }
    }

    /**
     * @param $searchTerm
     * @return string
     */
    public function getSearchTermType($searchTerm)
    {
        if (preg_match('/^\d{1,5}$/i', $searchTerm, $match)){
            return 'zipCode';
        }

        return 'locationName';
    }

    /**
     * @param null $locationName
     * @return array
     */
    public function getCoordinatesByName($locationName = NULL)
    {
        if (!is_null($locationName)) {
            $sql = sprintf('SELECT * FROM geodb_zip_coordinates WHERE zc_location_name LIKE "%%%s%%";', $locationName);

            $resultList = $this->execute($sql);

            return (!empty($resultList)) ? $resultList : [];
        }
    }

    /**
     * @param null $zipCode
     * @return array|mixed
     */
    public function getCoordinatesByZipCode($zipCode = NULL)
    {
        if (!is_null($zipCode)) {
            $sql = sprintf('SELECT * FROM geodb_zip_coordinates WHERE zc_zip LIKE "%s%%";', $zipCode);

            $resultList = $this->execute($sql);

            return (!empty($resultList)) ? $resultList : [];
        }
    }

    /**
     * @param $location_id
     * @param int $radius
     * @return mixed
     * @throws \Exception
     */
    public function getPeripheryDataByLocationIdAndRadius($location_id, $radius = 10, $max_population = NULL)
    {
        if (!is_int($radius)) {
            throw new \Exception('Wrong radius value. Must be an integer.');
        }

        $distance_alg = 'ACOS( SIN(RADIANS(src.zc_lat)) * SIN(RADIANS(dest.zc_lat)) + COS(RADIANS(src.zc_lat)) * COS(RADIANS(dest.zc_lat)) * COS(RADIANS(src.zc_lon) - RADIANS(dest.zc_lon)) ) * 6380 AS distance';
        $max_population = (!is_null($max_population)) ? sprintf(' AND dest.zc_population < %s', $max_population) : '';

        $sql = sprintf('SELECT dest.zc_zip, dest.zc_location_name, dest.zc_population, %s
                        FROM geodb_zip_coordinates dest CROSS JOIN geodb_zip_coordinates src
                        WHERE src.zc_id = %s
                        AND dest.zc_id <> src.zc_id
                        %s
                        HAVING distance < %s
                        ORDER BY distance;', $distance_alg, $location_id, $max_population, $radius);

        return $this->execute($sql);
    }

    /**
     * @param $locationId
     * @return mixed
     * @throws \Exception
     */
    public function getLocationById($locationId)
    {
        if (!is_numeric($locationId)) {
            throw new \Exception('Wrong id value. Must be an integer.');
        }

        $sql = sprintf('SELECT zc_zip as `zipcode`, zc_location_name as `location` FROM `geodb_zip_coordinates` WHERE zc_id=%s;', $locationId);
        return $this->execute($sql);
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function getLocationByName($name)
    {
        if (!is_string($name)) {
            throw new \Exception('Wrong name value. Must be a string.');
        }

        $sql = sprintf('SELECT zc_zip as `zipcode`, zc_location_name as `location` FROM `geodb_zip_coordinates` WHERE `zc_location_name` LIKE("%s%%");', $name);
        return $this->execute($sql);
    }

    /**
     * @param $zipcode
     * @return mixed
     * @throws \Exception
     */
    public function getLocationByZipCode($zipcode, $limit = 1)
    {
        if (!is_string($zipcode)) {
            throw new \Exception('Wrong zipcode value. Must be a string.');
        }

        $sql = sprintf('SELECT zc_id as `location_id`, zc_zip as `zipcode`, zc_location_name as `location` FROM `geodb_zip_coordinates` WHERE `zc_zip` LIKE("%s%%") LIMIT %s;', $zipcode, $limit);
        return $this->execute($sql);
    }

    /**
     * @param array $zipCodes
     * @return mixed
     * @throws \Exception
     */
    public function getLocationsByZipCodes(Array $zipCodes)
    {
        $sql = sprintf('SELECT zc_id, zc_zip, zc_location_name, zc_population FROM `geodb_zip_coordinates` WHERE `zc_zip` IN (%s);', implode($zipCodes));
        return $this->execute($sql);
    }
}

