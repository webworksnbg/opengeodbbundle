## Configuration

1. Setup and install a openGeoDb based on mySQL or mariaDB
2. Add the following doctrine configuration to your config.yml
 
```
#!yaml
 doctrine:
     dbal:
         default_connection: default
         connections:
           default:
             driver:   "%database_driver%"
             host:     "%database_host%"
             port:     "%database_port%"
             dbname:   "%database_name%"
             user:     "%database_user%"
             password: "%database_password%"
             charset:  UTF8
           opengeodb:
             driver:   "%database_driver%"
             host:     "%opengeodb_host%"
             port:     "%opengeodb_port%"
             dbname:   "%opengeodb_name%"
             user:     "%opengeodb_user%"
             password: "%opengeodb_password%"
             charset:  UTF8
     orm:
         auto_generate_proxy_classes: "%kernel.debug%"
         default_entity_manager: default
         entity_managers:
           default:
             connection: default
             mappings:
               ImmoRacerBundle: ~
           opengeodb:
             connection: opengeodb
```
