<?php

namespace OpenGeoDb\Helper;

class CollectionHelper
{
    /**
     * @param array $collection
     * @param $sourceTerm
     * @param $matchingFieldName
     * @return array
     */
    public static function addLevenshteinDistance(Array $collection, $sourceTerm, $matchingFieldName)
    {
        return array_map(function($item) use($sourceTerm, $matchingFieldName) {
            $item['levenshtein_dist'] = levenshtein ($sourceTerm, $item[$matchingFieldName]);
            return $item;
        }, $collection);
    }

    /**
     * @param array $collection
     * @param string $levenshteinFieldName
     * @return array
     */
    public static function sortByLeveshteinDistance(Array $collection, $levenshteinFieldName='levenshtein_dist')
    {
        usort ($collection, function($a, $b) use ($levenshteinFieldName) {
            if ($a[$levenshteinFieldName] == $b[$levenshteinFieldName]) {
                return 0;
            }
            return ($a[$levenshteinFieldName] < $b[$levenshteinFieldName]) ? -1 : 1;
        });

        return $collection;
    }
}
